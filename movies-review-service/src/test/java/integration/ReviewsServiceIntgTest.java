import java.util.List;

import com.reactivespring.model.ConstraintViolationsDto;
import com.reactivespring.model.Review;
import com.reactivespring.reposotiry.ReviewRepository;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import reactor.test.StepVerifier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest
@AutoConfigureWebTestClient
@Testcontainers
class ReviewsServiceIntgTest {

	private static final String MOVIE_REVIEW_URL = "/v1/reviews";

	@Container
	static MongoDBContainer mongoDB = new MongoDBContainer("mongo:latest");

	@Autowired
	WebTestClient webTestClient;

	@Autowired
	ReviewRepository reviewRepository;

	@DynamicPropertySource
	static void beforeAll(DynamicPropertyRegistry propertyRegistry) {
		propertyRegistry.add("spring.data.mongodb.uri", mongoDB::getReplicaSetUrl);
	}

	@BeforeEach
	void setUp() {
		reviewRepository.deleteAll().block();
	}

	@NotNull
	private static Review getReview1() {
		return new Review("rev1", 1L, "nice film", 10d);
	}

	@NotNull
	private static Review getReview2() {
		return new Review("rev2", 2L, "awesome", 100d);
	}

	@Test
	void givenValidReview_whenPostNewReview_thenReturnStatusIsCreatedAndValidLocation() {
		// Given:
		var review = getReview1();

		// When:
		var response = webTestClient.post()
				.uri(MOVIE_REVIEW_URL)
				.bodyValue(review)
				.exchange();

		// Then:
		response.expectStatus().isCreated()
				.expectHeader().location(MOVIE_REVIEW_URL + "/" + review.getReviewId());

		StepVerifier.create(reviewRepository.findById(review.getReviewId()))
				.expectNext(review)
				.verifyComplete();
	}


	@Test
	void givenInValidReview_whenPostNewReview_thenReturnStatusBadRequest() {
		// Given:
		var review = getReview1();

		// When:
		var response = webTestClient.post()
				.uri(MOVIE_REVIEW_URL)
				.bodyValue(review)
				.exchange();

		// Then:
		response.expectStatus().isBadRequest()
				.expectBody(ConstraintViolationsDto.class).consumeWith(responseBody -> {
							var constraintViolationsDto = responseBody.getResponseBody();
							Assertions.assertEquals(1, constraintViolationsDto.getConstraintViolations().size());
						}
				);
	}

	@Test
	void givenTwoReview_whenGetAllReview_thenReturnStatusIsOkAndReturnTwoReview() {
		// Given:
		var review1 = getReview1();
		var review2 = getReview2();
		reviewRepository.saveAll(List.of(review1, review2)).blockLast();

		// When:
		var response = webTestClient.get()
				.uri(MOVIE_REVIEW_URL)
				.exchange();

		// Then:
		response.expectStatus().isOk();

		StepVerifier.create(response.returnResult(Review.class).getResponseBody())
				.expectNext(review1)
				.expectNext(review2)
				.verifyComplete();
	}


	@Test
	void givenValidReviewId_whenGetReviewById_thenReturnStatusIsOkAndReturnReview() {
		// Given:
		var review1 = getReview1();
		reviewRepository.save(review1).block();
		var id = review1.getReviewId();

		// When:
		var response = webTestClient.get()
				.uri(uri -> uri.path(MOVIE_REVIEW_URL + "/{id}").build(id))
				.exchange();

		// Then:
		response.expectStatus().isOk()
				.expectBody(Review.class).isEqualTo(review1);
	}

	@Test
	void givenInValidReviewId_whenGetReviewById_thenReturnStatusNotFound() {
		// Given:
		var review1 = getReview1();
		reviewRepository.save(review1).block();
		var id = "rev100000";

		// When:
		var response = webTestClient.get()
				.uri(uri -> uri.path(MOVIE_REVIEW_URL + "/{id}").build(id))
				.exchange();

		// Then:
		response.expectStatus().isNotFound();
	}

	@Test
	void givenUpdatedReview_whenUpdateReview_thenReturnStatusIsOk() {
		// Given:
		var oldReview = getReview1();
		reviewRepository.save(oldReview).block();

		var id = oldReview.getReviewId();
		var updatedReview = new Review("rev1", 1L, "awesome", 100d);

		// When:
		var response = webTestClient.put()
				.uri(uri -> uri.path(MOVIE_REVIEW_URL + "/{id}").build(id))
				.bodyValue(updatedReview)
				.exchange();

		// Then:
		response.expectStatus().isOk();

		StepVerifier.create(reviewRepository.findById(id))
				.expectNext(updatedReview)
				.verifyComplete();
	}

	@Test
	void givenUpdatedReviewWithInValidId_whenUpdateReview_thenReturnStatusIsBadRequest() {
		// Given:
		var oldReview = getReview1();
		reviewRepository.save(oldReview).block();

		var id = "rev100000";
		var updatedReview = new Review("rev100000", 1L, "awesome", 100d);

		// When:
		var response = webTestClient.put()
				.uri(uri -> uri.path(MOVIE_REVIEW_URL + "/{id}").build(id))
				.bodyValue(updatedReview)
				.exchange();

		// Then:
		response.expectStatus().isBadRequest();
	}

}
