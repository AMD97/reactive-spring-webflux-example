package com.reactivespring.controller;


import java.net.URI;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import com.reactivespring.model.Review;
import com.reactivespring.exception.ReviewNotFoundException;
import com.reactivespring.exception.ValidationException;
import com.reactivespring.service.ReviewService;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

@Component
@RequiredArgsConstructor
public class ReviewHandler {

	private final Validator validator;
	private final ReviewService reviewService;

	public <E> Mono<E> validate(E object) {

		Set<ConstraintViolation<E>> constraintViolations = validator.validate(object);
		if (!constraintViolations.isEmpty()) {
			var msg = constraintViolations.stream()
					.map(ConstraintViolation::getMessage)
					.toList();
			return Mono.error(new ValidationException(msg));
		} else {
			return Mono.just(object);
		}
	}

	Mono<ServerResponse> addReview(ServerRequest request) {

		return request.bodyToMono(Review.class)
				.flatMap(this::validate)
				.flatMap(reviewService::save)
				.flatMap(review -> ServerResponse.created(URI.create("/v1/reviews/" + review.getReviewId()))
						.contentType(MediaType.APPLICATION_JSON)
						.build());
	}

	public Mono<ServerResponse> getAllReview(ServerRequest request) {

		return ServerResponse.ok()
				.body(reviewService.getAllReview(), Review.class);
	}

	public Mono<ServerResponse> updateReview(ServerRequest request) {

		String id = request.pathVariable("id");
		return request.bodyToMono(Review.class)
				.flatMap(review -> reviewService.update(id, review))
				.flatMap(review -> ServerResponse.ok().build())
				.onErrorResume(ReviewNotFoundException.class, err -> ServerResponse.badRequest().build());

	}

	public Mono<ServerResponse> getReview(ServerRequest request) {

		String id = request.pathVariable("id");
		return reviewService.getReview(id)
				.flatMap(review -> ServerResponse.ok().bodyValue(review))
				.onErrorResume(ReviewNotFoundException.class, err -> ServerResponse.notFound().build());
	}

}
