package com.reactivespring.controller;

import com.reactivespring.model.ConstraintViolationsDto;
import com.reactivespring.exception.ValidationException;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

@Component
@Slf4j
public class ValidationExceptionHandler {

	public Mono<ServerResponse> handle(ValidationException ex, ServerRequest serverRequest) {

		var responseBuilder = ServerResponse
				.badRequest()
				.contentType(MediaType.APPLICATION_JSON);

		var constraintViolationsDto = new ConstraintViolationsDto(ex.getConstraintViolations());

		return Mono.defer(() -> responseBuilder.bodyValue(constraintViolationsDto));
	}

}

