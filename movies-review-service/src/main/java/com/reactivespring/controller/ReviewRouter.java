package com.reactivespring.controller;

import com.reactivespring.exception.ValidationException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.path;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class ReviewRouter {

	@Bean
	public RouterFunction<ServerResponse> reviewRoute(
			ReviewHandler reviewHandler,
			ValidationExceptionHandler validationExceptionHandler
	) {
		return route()
				.nest(
						path("/v1/reviews"), base -> base
								.POST("", reviewHandler::addReview)
								.GET("", reviewHandler::getAllReview)
								.GET("/{id}", reviewHandler::getReview)
								.PUT("/{id}", reviewHandler::updateReview)
				)
				.onError(ValidationException.class, validationExceptionHandler::handle)
				.build();
	}

}
