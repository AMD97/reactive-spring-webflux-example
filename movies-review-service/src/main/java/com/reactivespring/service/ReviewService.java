package com.reactivespring.service;

import com.reactivespring.model.Review;
import com.reactivespring.exception.ReviewNotFoundException;
import com.reactivespring.reposotiry.ReviewRepository;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ReviewService {

	private final ReviewRepository reviewRepository;

	public Flux<Review> getAllReview() {
		return reviewRepository.findAll();
	}

	public Mono<Review> save(Review review) {
		return reviewRepository.save(review);
	}

	public Mono<Review> update(String id, Review review) {

		return reviewRepository.findById(id)
				.switchIfEmpty(Mono.error(() -> new ReviewNotFoundException("Review not found")))
				.flatMap(retrivedReview -> {
					review.setReviewId(id);
					return reviewRepository.save(review);
				});
	}

	public Mono<Review> getReview(String id) {

		return reviewRepository.findById(id)
				.switchIfEmpty(Mono.error(() -> new ReviewNotFoundException("Review not found")));
	}
}
