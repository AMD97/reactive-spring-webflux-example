package com.reactivespring.exception;

import java.util.List;

public class ValidationException extends RuntimeException {

	private final List<String> constraintViolations;

	public ValidationException(List<String> constraintViolations) {
		this.constraintViolations = constraintViolations;
	}

	public ValidationException(String message, List<String> constraintViolations) {
		super(message);
		this.constraintViolations = constraintViolations;
	}

	public List<String> getConstraintViolations() {
		return constraintViolations;
	}

}
