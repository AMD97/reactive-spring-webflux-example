package com.reactivespring.exception;

public class ReviewNotFoundException extends RuntimeException {

    private String message;
    private Throwable ex;

    public ReviewNotFoundException(String message) {
        super(message);
        this.message = message;
    }
    public ReviewNotFoundException( String message, Throwable cause) {
        super(message, cause);
    }

    public ReviewNotFoundException(Throwable cause) {
        super(cause);
    }

}
