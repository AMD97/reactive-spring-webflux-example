import java.time.LocalDate;
import java.util.List;

import com.example.model.MovieInfo;
import com.example.repository.MovieInfoRepository;
import org.assertj.core.api.Assertions;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import reactor.test.StepVerifier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureWebTestClient
@Testcontainers
class MovieInfoControllerIntegrationTests {

	private static final String MOVIE_INFO_URL = "/v1/movieInfo";
	@Container
	static MongoDBContainer mongoDB = new MongoDBContainer("mongo:latest");
	@Autowired
	WebTestClient webTestClient;

	@Autowired
	MovieInfoRepository movieInfoRepository;

	@DynamicPropertySource
	static void setupProperties(DynamicPropertyRegistry propertyRegistry) {
		propertyRegistry.add("spring.data.mongodb.uri", mongoDB::getReplicaSetUrl);
	}

	@NotNull
	private static MovieInfo getMovieInfo1() {
		return new MovieInfo("batman", "Batman", 2019, List.of("Christian Bale"), LocalDate.now());
	}

	@NotNull
	private static MovieInfo getMovieInfo2() {
		return new MovieInfo("spider-man", "Spider Man", 2016, List.of("Tom Holland"), LocalDate.now());
	}

	@Test
	void givenTwoMoviesInfo_whenGetAllMoviesInfo_thenReturnOkStatusAndReturnTwoMovieInfo() {
		// Given:
		var movieInfo1 = getMovieInfo1();
		var movieInfo2 = getMovieInfo2();
		movieInfoRepository.saveAll(List.of(movieInfo1, movieInfo2)).blockLast();

		// When:
		var response = webTestClient.get()
				.uri(MOVIE_INFO_URL)
				.exchange();

		// Then:
		response.expectStatus().isOk();

		StepVerifier.create(response.returnResult(MovieInfo.class).getResponseBody())
				.expectNext(movieInfo1)
				.expectNext(movieInfo2)
				.verifyComplete();
	}

	@Test
	void givenValidMoviesInfoId_whenGetMoviesInfoById_thenReturnOkStatusAndReturnMovieInfo() {
		// Given:
		var movieInfo1 = getMovieInfo1();
		movieInfoRepository.save(movieInfo1).block();

		// When:
		var response = webTestClient.get()
				.uri(uri -> uri.path(MOVIE_INFO_URL + "/{movieInfoId}").build(movieInfo1.getMovieInfoId()))
				.exchange();

		// Then:
		response.expectStatus().isOk()
				.expectBody(MovieInfo.class).isEqualTo(movieInfo1);
	}

	@Test
	void givenInvalidMovieInfoId_whenGetMovieInfo_thenReturnStatusNotfound() {
		// Given:
		var invalidId = "abcd";

		// When:
		var response = webTestClient.get()
				.uri(uri -> uri.path(MOVIE_INFO_URL + "/{movieInfoId}").build(invalidId))
				.exchange();

		// Then:
		response.expectStatus().isNotFound();
	}

	@Test
	void givenValidMovieInfo_whenPostMovieInfo_thenReturnCreatedStatusAndRepositoryReturnMovieInfo() {
		// Given:
		var movieInfo = getMovieInfo1();

		// When:
		var response = webTestClient.post()
				.uri(MOVIE_INFO_URL + "")
				.bodyValue(movieInfo)
				.exchange();

		// Then:
		response.expectStatus().isCreated()
				.expectBody(MovieInfo.class).isEqualTo(movieInfo);

		Assertions.assertThat(movieInfoRepository.findById(movieInfo.getMovieInfoId()).block())
				.isEqualTo(movieInfo);
	}

	@Test
	void givenNewMovieInfo_whenUpdateMovieInfo_thenReturnCreatedAcceptedAndRepositoryReturnUpdatedMovieInfo() {
		// Given:
		var movieInfo = getMovieInfo1();
		movieInfoRepository.save(movieInfo).block();

		var newMovieInfo = new MovieInfo("batman", "Batman", 2010, List.of("Christian Bale"), LocalDate.now());

		// When:
		var response = webTestClient.put()
				.uri(uri -> uri.path(MOVIE_INFO_URL + "/{movieInfoId}").build(movieInfo.getMovieInfoId()))
				.bodyValue(newMovieInfo)
				.exchange();

		// Then:
		response.expectStatus().isAccepted()
				.expectBody(MovieInfo.class).isEqualTo(newMovieInfo);

		Assertions.assertThat(movieInfoRepository.findById(movieInfo.getMovieInfoId()).block())
				.isEqualTo(newMovieInfo);
	}

	@Test
	void givenCorrectMoviesInfoId_whenDeleteMoviesInfoById_thenReturnOkStatusAndReturnMovieInfo() {
		// Given:
		var movieInfo1 = getMovieInfo1();
		movieInfoRepository.save(movieInfo1).block();

		// When:
		var response = webTestClient.delete()
				.uri(uri -> uri.path(MOVIE_INFO_URL + "/{movieInfoId}").build(movieInfo1.getMovieInfoId()))
				.exchange();

		// Then:
		response.expectStatus().isOk();

		Assertions.assertThat(movieInfoRepository.findById(movieInfo1.getMovieInfoId()).block())
				.isNull();
	}
}