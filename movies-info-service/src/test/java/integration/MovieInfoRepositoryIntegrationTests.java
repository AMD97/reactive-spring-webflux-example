import java.time.LocalDate;
import java.util.List;

import com.example.model.MovieInfo;
import com.example.repository.MovieInfoRepository;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import reactor.test.StepVerifier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;


@DataMongoTest(excludeAutoConfiguration = EmbeddedMongoAutoConfiguration.class)
@Testcontainers
class MovieInfoRepositoryIntegrationTests {

	@Container
	static MongoDBContainer mongoDB = new MongoDBContainer("mongo:latest")
			.withReuse(true);

	@Autowired
	MovieInfoRepository movieInfoRepository;

	@DynamicPropertySource
	static void setupProperties(DynamicPropertyRegistry dynamicProperty) {
		dynamicProperty.add("spring.data.mongodb.uri", mongoDB::getReplicaSetUrl);
	}

	@NotNull
	private static MovieInfo getMovieInfo1() {
		return new MovieInfo("batman", "Batman", 2019, List.of("Christian Bale"), LocalDate.now());
	}
	@NotNull
	private static MovieInfo getMovieInfo2() {
		return new MovieInfo("spider-man", "Spider Man", 2016, List.of("Tom Holland"), LocalDate.now());
	}

	@Test
	void addAndGet() {
		// Given:
		var movieInfo1 = new MovieInfo("batman", "Batman", 2019, List.of("Christian Bale"), LocalDate.now());
		movieInfoRepository.save(movieInfo1).block();

		// When:
		var moviesInfoFlux = movieInfoRepository.findAll();

		// Then:
		StepVerifier.create(moviesInfoFlux)
				.expectNext(movieInfo1)
				.verifyComplete();
	}

	@Test
	void delete() {
		// Given:
		var movieInfo1 = new MovieInfo("batman", "Batman", 2019, List.of("Christian Bale"), LocalDate.now());
		movieInfoRepository.save(movieInfo1).block();

		// When:
		movieInfoRepository.deleteById("batman").block();
		var moviesInfoFlux = movieInfoRepository.findAll();

		// Then:
		StepVerifier.create(moviesInfoFlux)
				.verifyComplete();
	}

	@Test
	void findByYear() {
		// Given:
		var movieInfo1 = new MovieInfo("batman", "Batman", 2019, List.of("Christian Bale"), LocalDate.now());
		var movieInfo2 = new MovieInfo("spider-man", "Spider Man", 2016, List.of("Tom Holland"), LocalDate.now());
		movieInfoRepository.saveAll(List.of(movieInfo1, movieInfo2)).blockLast();

		// When:
		movieInfoRepository.findMovieInfoByYear(2019).log().blockLast();
		var moviesInfoFlux = movieInfoRepository.findAll();

		// Then:
		StepVerifier.create(moviesInfoFlux)
				.expectNext(movieInfo1)
				.verifyComplete();
	}

}