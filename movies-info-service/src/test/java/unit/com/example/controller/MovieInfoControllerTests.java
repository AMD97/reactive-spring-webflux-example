package com.example.controller;

import java.time.LocalDate;
import java.util.List;

import com.example.exception.EntityNotFoundException;
import com.example.model.MovieInfo;
import com.example.service.MovieInfoService;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebFlux;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@WebFluxTest(controllers = MovieInfoController.class)
@AutoConfigureWebFlux
class MovieInfoControllerTests {

	private static final String MOVIE_INFO_URL = "/v1/movieInfo";

	@Autowired
	WebTestClient webTestClient;

	@MockBean
	MovieInfoService movieInfoServiceMock;

	@NotNull
	private static MovieInfo getMovieInfo1() {
		return new MovieInfo("batman", "Batman", 2019, List.of("Christian Bale"), LocalDate.now());
	}
	@NotNull
	private static MovieInfo getMovieInfo2() {
		return new MovieInfo("spider-man", "Spider Man", 2016, List.of("Tom Holland"), LocalDate.now());
	}

	@Test
	void givenTwoMovieInfo_whenGetAllMovieInfo_thenReturnTwoMovieInfo() {
		// Given:
		var movieInfo1 = getMovieInfo1();
		var movieInfo2 = getMovieInfo2();
		given(movieInfoServiceMock.getAllMoviesInfo())
				.willReturn(Flux.just(movieInfo1, movieInfo2));

		// When:
		var response = webTestClient
				.get()
				.uri(MOVIE_INFO_URL)
				.exchange();

		// Then:
		response.expectStatus().isOk();

		StepVerifier.create(response.returnResult(MovieInfo.class).getResponseBody())
				.expectNext(movieInfo1)
				.expectNext(movieInfo2)
				.verifyComplete();
	}

	@Test
	void givenValidMovieInfo_whenPostMovieInfo_thenReturnStatusCreatedAndReturnMovieInfo() {
		// Given:
		var movieInfo1 = getMovieInfo1();

		var argCaptor = ArgumentCaptor.forClass(MovieInfo.class);
		given(movieInfoServiceMock.addMovieInfo(argCaptor.capture()))
				.willReturn(Mono.fromCallable(argCaptor::getValue));

		// When:
		var response = webTestClient
				.post()
				.uri(MOVIE_INFO_URL)
				.bodyValue(movieInfo1)
				.exchange();

		// Then:
		response.expectStatus().isCreated()
				.expectBody(MovieInfo.class).isEqualTo(movieInfo1);
	}

	@Test
	void givenInvalidMovieInfo_whenPostMovieInfo_thenReturnStatusBadRequest() {
		// Given:
		var movieInfo1 = getMovieInfo1();

		var argCaptor = ArgumentCaptor.forClass(MovieInfo.class);
		given(movieInfoServiceMock.addMovieInfo(argCaptor.capture()))
				.willReturn(Mono.fromCallable(argCaptor::getValue));

		// When:
		var response = webTestClient
				.post()
				.uri(MOVIE_INFO_URL)
				.bodyValue(movieInfo1)
				.exchange();

		// Then:
		response.expectStatus().isBadRequest();
	}

	@Test
	void givenValidMovieInfoId_whenGetMovieInfo_thenReturnStatusOKAndReturnMovieInfo() {
		// Given:
		var movieInfo1 = getMovieInfo1();

		given(movieInfoServiceMock.getMovieInfoById(any()))
				.willReturn(Mono.just(movieInfo1));

		// When:
		var response = webTestClient
				.get()
				.uri(uri-> uri.path(MOVIE_INFO_URL + "/{id}").build(movieInfo1.getMovieInfoId()))
				.exchange();

		// Then:
		response.expectStatus().isOk()
				.expectBody(MovieInfo.class).isEqualTo(movieInfo1);
	}

	@Test
	void givenInValidMovieInfoId_whenGetMovieInfo_thenReturnStatusNotfound() {
		// Given:
		given(movieInfoServiceMock.getMovieInfoById(any()))
				.willReturn(Mono.error(EntityNotFoundException::new));

		// When:
		var response = webTestClient
				.get()
				.uri(uri-> uri.path(MOVIE_INFO_URL + "/{id}").build("invalidId"))
				.exchange();

		// Then:
		response.expectStatus().isNotFound();
	}

}