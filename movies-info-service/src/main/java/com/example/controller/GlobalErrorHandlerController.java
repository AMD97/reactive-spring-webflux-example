package com.example.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.support.WebExchangeBindException;

import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class GlobalErrorHandlerController {

	@ExceptionHandler(WebExchangeBindException.class)
	public ResponseEntity<String> handleRequestBodyValidationError(WebExchangeBindException ex) {

		log.error("Exception caught in handleRequestBodyError: {} \n {}", ex.getMessage(), ex);

		String validationError =
				ex.getBindingResult().getAllErrors()
						.stream()
						.map(DefaultMessageSourceResolvable::getDefaultMessage)
						.sorted()
						.collect(Collectors.joining(", "));

		log.info("Validation Error: {}", validationError);

		return ResponseEntity.badRequest()
					   .body(validationError);
	}

}
