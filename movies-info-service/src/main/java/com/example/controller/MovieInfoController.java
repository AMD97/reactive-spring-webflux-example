package com.example.controller;

import com.example.model.MovieInfo;
import com.example.exception.EntityNotFoundException;
import com.example.service.MovieInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1/movieInfo")
@RequiredArgsConstructor
public final class MovieInfoController {

	private final MovieInfoService movieInfoService;

	@GetMapping
	public Flux<MovieInfo> getAllMoviesInfo() {
		return movieInfoService.getAllMoviesInfo();
	}

	@GetMapping("/{movieInfoId}")
	public Mono<ResponseEntity<MovieInfo>> getMovieInfoById(@PathVariable String movieInfoId) {
		return movieInfoService.getMovieInfoById(movieInfoId)
					   .map(ResponseEntity.ok()::body)
					   .onErrorReturn(EntityNotFoundException.class, ResponseEntity.notFound().build());
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Mono<MovieInfo> addMovieInfo(@RequestBody @Valid MovieInfo movieInfo) {
		return movieInfoService.addMovieInfo(movieInfo);
	}

	@PutMapping("{movieInfoId}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public Mono<MovieInfo> updateMovieInfo(
			@PathVariable String movieInfoId,
			@RequestBody MovieInfo movieInfo
	) {
		return movieInfoService.updateMovieInfo(movieInfoId, movieInfo);
	}

	@DeleteMapping("/{movieInfoId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public Mono<Void> deleteMovieInfoById(@PathVariable String movieInfoId) {
		return movieInfoService.deleteMovieInfoById(movieInfoId);
	}

}
