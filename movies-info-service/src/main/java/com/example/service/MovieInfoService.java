package com.example.service;

import com.example.model.MovieInfo;
import com.example.exception.EntityNotFoundException;
import com.example.repository.MovieInfoRepository;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MovieInfoService {

	private final MovieInfoRepository movieInfoRepository;

	public Mono<MovieInfo> addMovieInfo(MovieInfo movieInfo) {
		return movieInfoRepository.save(movieInfo);
	}

	public Flux<MovieInfo> getAllMoviesInfo() {
		return movieInfoRepository.findAll();
	}

	public Mono<MovieInfo> getMovieInfoById(String movieInfoId) {
		return movieInfoRepository.findById(movieInfoId)
				.switchIfEmpty(Mono.error(EntityNotFoundException::new));
	}

	public Mono<MovieInfo> updateMovieInfo(String movieInfoId, MovieInfo movieInfo) {
		return movieInfoRepository.findById(movieInfoId)
				.flatMap(movieInfoRetried -> movieInfoRepository.save(movieInfo));
	}

	public Mono<Void> deleteMovieInfoById(String movieInfoId) {
		return movieInfoRepository.deleteById(movieInfoId);
	}

	public MovieInfoRepository movieInfoRepository() {
		return movieInfoRepository;
	}

}
