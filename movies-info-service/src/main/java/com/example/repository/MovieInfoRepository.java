package com.example.repository;

import com.example.model.MovieInfo;
import reactor.core.publisher.Flux;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieInfoRepository
		extends ReactiveMongoRepository<MovieInfo, String> {

	Flux<MovieInfo> findMovieInfoByYear(Integer year);

}
