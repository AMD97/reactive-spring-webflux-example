import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.reactivespring.model.Movie;
import com.reactivespring.model.MovieInfo;
import com.reactivespring.model.Review;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.reactive.server.WebTestClient;

import static com.github.tomakehurst.wiremock.client.WireMock.badRequest;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.ok;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureWebTestClient
class MoviesServiceIntgTest {

	@Value("/${api.movieInfo.version}/reviews")
	static final String MOVIE_INFO_PATH = "/v1/movieInfo";

	@Value("/${api.movieReview.version}/reviews")
	static String MOVIE_REVIEW_PATH = "/v1/reviews";

	@RegisterExtension
	static WireMockExtension movieInfoMockServer = WireMockExtension.newInstance()
			.options(wireMockConfig().dynamicPort()).build();
	@RegisterExtension
	static WireMockExtension movieReviewMockServer = WireMockExtension.newInstance()
			.options(wireMockConfig().dynamicPort()).build();

	@Autowired
	WebTestClient webTestClient;

	@Autowired
	ObjectMapper mapper;

	@DynamicPropertySource
	static void setUpProprieties(DynamicPropertyRegistry propertyRegistry) {
		propertyRegistry.add("api.movieInfo.baseUrl", movieInfoMockServer::baseUrl);
		propertyRegistry.add("api.movieReview.baseUrl", movieReviewMockServer::baseUrl);
	}

	private static MovieInfo getMovieInfo1() {
		return new MovieInfo("batman", "Batman", 2019, List.of("Christian Bale"), LocalDate.now());
	}

	private static Review getReview1() {
		return new Review("rev1", "mov1", "nice film", 10d);
	}

	@Test
	void givenValidMovieInfoAndMovieReviewApi_whenGetMovie_thenReturnOkAndReturnResult() throws Exception {
		// Given:
		var movieInfo = getMovieInfo1();
		var movieId = "mov1";
		movieInfo.setMovieInfoId(movieId);

		var movieReview = getReview1();
		movieReview.setMovieInfoId(movieId);

		movieInfoMockServer.givenThat(get(MOVIE_INFO_PATH + "/" + movieId)
				.willReturn(ok()
						.withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
						.withBody(mapper.writeValueAsBytes(movieInfo)))
		);
		movieReviewMockServer.givenThat(get(MOVIE_REVIEW_PATH + "/" + movieId)
				.willReturn(ok()
						.withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
						.withBody(mapper.writeValueAsBytes(List.of(movieReview))))
		);

		// When:
		var response = webTestClient.get()
				.uri("/v1/movies/{movieId}", movieId)
				.exchange();

		// Then:
		response.expectStatus().isOk()
				.expectBody(Movie.class).consumeWith(movieEntity -> {
					assertThat(movieEntity.getResponseBody().getMovieInfo())
							.isEqualTo(movieInfo);

					assertThat(movieEntity.getResponseBody().getReviewList())
							.contains(movieReview);
				});

	}

	@Test
	void givenInValidMovieInfoAndMovieReviewApi_whenGetMovie_thenReturnBadRequest() throws JsonProcessingException {
		// Given:
		var movieInfo = getMovieInfo1();
		var movieId = "mov1";
		movieInfo.setMovieInfoId(movieId);

		movieInfoMockServer.givenThat(get(MOVIE_INFO_PATH + "/" + movieId)
				.willReturn(ok()
						.withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
						.withBody(mapper.writeValueAsBytes(movieInfo)))
		);
		movieReviewMockServer.givenThat(get(MOVIE_REVIEW_PATH + "/" + movieId)
				.willReturn(badRequest()));

		// When:
		var response = webTestClient.get()
				.uri("/v1/movies/{movieId}", movieId)
				.exchange();

		// Then:
		response.expectStatus().isNotFound();

	}

}
