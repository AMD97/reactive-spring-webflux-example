package com.reactivespring.service;

import com.reactivespring.model.Movie;
import com.reactivespring.webApi.MovieReviewRestClient;
import com.reactivespring.webApi.MoviesInfoRestClient;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MovieService {
	private final MoviesInfoRestClient moviesInfoRestClient;
	private final MovieReviewRestClient movieReviewRestClient;
	public Mono<Movie> getMovie(String movieId) {

		return Mono.zip(
				moviesInfoRestClient.getMovieInfo(movieId),
				movieReviewRestClient.getMovieReviewById(movieId).collectList(),
				Movie::new
		);
	}

}
