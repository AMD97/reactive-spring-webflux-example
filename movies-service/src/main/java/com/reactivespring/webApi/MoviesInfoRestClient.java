package com.reactivespring.webApi;

import com.reactivespring.model.MovieInfo;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
@RequiredArgsConstructor
public class MoviesInfoRestClient {
	private final WebClient webClient;

	@Value("${api.movieInfo.baseUrl}")
	private String baseUrl;

	@Value("${api.movieReview.version}")
	private String version;

	public Mono<MovieInfo> getMovieInfo(String movieId) {

		String url = baseUrl
				.concat("/" + version)
				.concat("/movieInfo/{id}");
		return webClient.get()
				.uri(url, movieId)
				.retrieve()
				.bodyToMono(MovieInfo.class);
	}
}
