package com.reactivespring.webApi;

import com.reactivespring.model.Review;
import com.reactivespring.exception.MoviesInfoClientException;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
@RequiredArgsConstructor
public class MovieReviewRestClient {

	private final WebClient webClient;

	@Value("${api.movieReview.baseUrl}")
	private String baseUrl;

	@Value("${api.movieReview.version}")
	private String version;

	public Flux<Review> getMovieReviewById(String id) {

		String url = baseUrl.concat("/" + version)
				.concat("/reviews/{id}");

		return webClient.get()
				.uri(url, id)
				.retrieve()
				.onStatus(HttpStatus::is4xxClientError, clientResponse -> {
					return Mono.error(
							new MoviesInfoClientException(
									"MovieInfo not found, Requested MovieInfoId: " + id,
									HttpStatus.NOT_FOUND.value()
							)
					);
				})
				.bodyToFlux(Review.class);
	}

}
