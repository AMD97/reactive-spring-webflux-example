package com.reactivespring.controller;

import com.reactivespring.model.Movie;
import com.reactivespring.exception.MoviesInfoClientException;
import com.reactivespring.service.MovieService;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/movies")
@RequiredArgsConstructor
public class MovieController {
	private final MovieService movieService;

	@GetMapping("/{movieId}")
	public Mono<ResponseEntity<Movie>> getMovieById(@PathVariable String movieId) {

		return movieService.getMovie(movieId)
				.map(ResponseEntity::ok)
				.onErrorReturn(MoviesInfoClientException.class, ResponseEntity.notFound().build());
	}

}
